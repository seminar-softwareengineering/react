import logo from './logo.svg';
import './App.css';
import CustomerList from './customer-list';

/**
 * Hauptkomponente der Anwendung
 */
function App() {

  return (
    <CustomerList/>
  );
}

export default App;

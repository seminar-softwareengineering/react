import React from 'react';

/**
 * Komponente repräsentiert einen Kunden
 * Es werden die React-Properties customer.name und customer.city erwartet
 */
class Customer extends React.Component {
    render() {
        return ( <div>
                    <h3>{this.props.customer.name}</h3>
                    <span>{this.props.customer.city}</span>
                </div>
        );
    }
}

export default Customer;
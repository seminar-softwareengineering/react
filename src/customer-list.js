import React from 'react';
import Customer from './customer';

/**
 * Komponente repräsentiert eine Liste von Kunden
 */
class CustomerList extends React.Component {

    render() {
        // Für dieses Beispiel werden die Daten als Konstanten angelegt. Üblicherweise würde man diese aus einem REST-Service laden.
        const customer1 = { "name": "SoftDev GmbH", "city": "München" };
        const customer2 = { "name": "BIM AG", "city": "Berlin" };

        return (    <div>
                        <h1>Kunden:</h1>
                        <Customer customer={customer1} />
                        <Customer customer={customer2} />
                    </div>
        );
    }
}

export default CustomerList;